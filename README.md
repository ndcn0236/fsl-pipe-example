This is an example diffusion MRI pipeline using [fsl-pipe](https://git.fmrib.ox.ac.uk/ndcn0236/fsl-pipe).
A tutorial for fsl-pipe can be found [here](https://open.win.ox.ac.uk/pages/ndcn0236/fsl-pipe/).

Note that this pipeline is designed as an example of how to use fsl-pipe.
It does not represent the best way to analyse diffusion data.

# Pipeline overview
The whole pipeline is defined by two files:
- "data.tree": defines the directory structure of the data input and output in the [file-tree format](https://open.win.ox.ac.uk/pages/ndcn0236/file-tree/). The structure of the input data matches that of the [Human Connectome Project (HCP) dataset](https://www.humanconnectome.org/study/hcp-young-adult), however it can be easily adjusted to match your own data (see below).
- "pipe.py": defines a set of recipes that describe the different parts of the pipeline. Every recipe is a python function with the relevant inputs and outputs marked.
To understand the structure of the pipeline, I would recommend to open these two files side-by-side and read through the detailed comments in "pipe.py".

# Running the pipeline
## Setting up the environment
The first step is to clone this repository:
```bash
git clone https://git.fmrib.ox.ac.uk/ndcn0236/fsl-pipe-example.git
cd fsl-pipe-example
```
Then we need to install the requirements needed to run this pipeline (included in "requirements.txt"):
```bash
python -m pip install -r requirements.txt
```
This will install any python dependencies into the main python environment.

## Running the pipeline
I have provided an input folder matching the expected file structure (defined in "data.tree").
This means that after the setup above, you should be able to run the pipeline using:
```bash
python pipe/main.py
```
If run on the cluster, this will submit the whole diffusion pipeline in individual jobs to the cluster (with the correct dependencies between jobs).
This does require [fsl-sub](https://git.fmrib.ox.ac.uk/fsl/fsl_sub) to be set up correctly (see this [help to set this up](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslSge)).

If this command is run on a laptop or desktop, it will simply run all the jobs in order.

Note that while the jobs will be submitted, they will crash immediately, because the input data directory does not contain any actual data.
You can fix that by downloading some HCP raw data and putting it into the "input" folder.

## Adapting the pipeline to your own data
One option is to copy your data to the input folder and altering it to match the HCP file format.
However, I would recommend to instead create a symlink called "input" from this directory to your data.
You can then adjust "data.tree" to describe how your data is stored.
You can also adjust the output directory structure in "data.tree" as you wish.
You might have to adjust the pipeline ("pipe.py") to work with the pecularities of your dataset.
