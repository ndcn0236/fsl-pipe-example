#!/usr/bin/env sh

mkdir input
for subject in 100307 103818 ; do
    directory=input/${subject}/unprocessed/3T/Diffusion
    mkdir -p ${directory}
    for dir in 95 96 97 ; do
        for pe in LR RL ; do
            touch ${directory}/${subject}_3T_DWI_dir${dir}_${pe}.nii.gz
            touch ${directory}/${subject}_3T_DWI_dir${dir}_${pe}.bval
            touch ${directory}/${subject}_3T_DWI_dir${dir}_${pe}.bvec
        done
    done
done
